/*
 * Auther - Jack Warner-Pankhurst - 100199337
 * File - MatrixShortestDist.java
 * Last updated - 27/11/2018
 * Program - Calcuates the shortest distances between cities, using a 2 
 *           dimensional array storing the distances
           - ShortestDist is n^2 looking at every element
           - ShortestDistEF is n^2/2 utalising the fact the array is symmetrical
*/
package matrixshortestdist;
import java.util.*;


public class MatrixShortestDist {

    public static void main(String[] args) 
    {
       // TEST DISTANCES
//        int[][] matrix = new int[][]{
//                {0,58,184,271,378,379},//58
//                {58,0,167,199,351,382},//58
//                {184,167,0,43,374,370},//43
//                {271,199,43,0,394,390},//43
//                {378,351,374,394,0,47},//47
//                {379,382,370,390,47,0}//47
//        };
        
        int startsize = 1000; //cities to test

        int iterations = 10;
        long average1 = 0;
        long average2 = 0;
        ArrayList<Long> results1 = new ArrayList<>();
        ArrayList<Long> results2 = new ArrayList<>();
        
        //fill array with random symetrical distances  
        
        for (int i = startsize; i < 11000; i+=1000) //increase size to 100000
        {
            for (int k = 0; k < iterations; k++)//run 10 times for each size
            {
                //------------------TIMING FOR NORMAL MATRIX METHOD
                    int matrix[][] = fillArray(i);
                    long start1 = System.nanoTime();
                    ShortestDist(matrix,i);
                    long end1 = System.nanoTime();
                    average1 += (end1-start1); 
                    
                //-----------------------TIMING FOR EFFICIENT MATRIX METHOD
                    matrix = fillArray(i);
                    long start2 = System.nanoTime();
                    ShortestDistEF(matrix,i);
                    long end2 = System.nanoTime();
                    average2 += (end2-start2);
            }
            average1 /= (long)iterations;//calculate average accross runs
            average2 /= (long)iterations;
            results1.add(average1);//add results to lists
            results2.add(average2);
        }
        
        for(long l : results1)
        {
            System.out.print(l+" "); //print matrix timing results
        }
        System.out.println("\n");
        for (long o : results2)
        {
            System.out.print(o + " "); //print efficient matrix results
        }
        
    }
    /**
     * Calculates the shortest distances between cities in a two dimensional 
     * array of distances, where the y and x axes correlate to the cities
     * @param d : int that determines dimensions of array
     * @param cities : number of cities (dimensions) in array
     */
    public static void ShortestDist(int[][] d, int cities)
    {
        int results[] = new int[cities];//stores results for no. of cities
        
        for (int i = 0; i < cities; i++)//index through column
        {
            for (int j = 0; j < cities; j++)//index through row 
            {
                if (i != j) //check for same city comparison
                {
                    if (results[i] == 0) //if current shortst value is 0
                    {
                        results[i] = d[i][j]; //set to current value
                    }
                    //otherise if current value is shorter than stored shortest
                    else if(d[i][j] < results[i])
                    {
                        results[i] = d[i][j]; //overwite current shorest
                    }
                }
            }
        }
/*
        for (int i : results) //for every element in results
        {
            System.out.print(i+", ");//print distance
        }
*/
    }
    /**
     * Calculates the shortest distances between cities in a two dimensional 
     * symmetrical array, where the x and y axes correspond to the cities
     * @param d : symmetrical array to calculate shortest distances 
     * @param cities : number of cities (dimensions) in array
     */
    public static void ShortestDistEF(int[][] d, int cities)
    {
        int[] results = new int[cities];
        Arrays.fill(results, 6000); //fill array so dont have to check for 0
        for (int i = 0; i < cities; i++) //for each row 
        {
            for (int j = i+1; j < cities; j++) //look at number past i (non-0)
            {
                if(d[i][j] < results[i])
                {//if current val < current closest distance on row i
                    results[i] = d[i][j];
  
                }
                if(d[j][i] < results[j])
                {//if current val < current closest on row j
                    results[j] = d[i][j];
                }
            }
        } 
/*----------------------------------------------PRINT RESULTS
        for (int i : results) //for every element in results
        {
            System.out.print(i+", ");//print distance
        }
        System.out.println("\n");
*/
    }
    /**
     * Fills and array with random distances for a given number of cities as
     * its dimensions. Array is symmetrical.
     * @param size : int that determines dimensions of array
     * @return array[][] : filled symmetrical int array
     */
    public static int[][] fillArray(int size)
    {
        int[][] a = new int[size][size]; //create 2 dimensional array to size
        
        Random r = new Random();

        for (int i = 0; i < size; i++) //for each row
        {
            for (int j = 0; j <size; j++)//for each column
            {
                if (i == j)
                {
                    a[i][j] = 0; //add 0 if i & j are equal (on symetry line)
                }
                else
                {
                    int ra = r.nextInt(500); //max random value of 499
                    a[i][j] = ra; //add same no to a at row and column i and j
                    a[j][i] = ra;
                }
            }
        }
/*-----------------------------------------------TEST PRINT OF GENERATED MATRIX
        for (int k[] : a)
        {
            for (int l : k)
            {
                System.out.printf("%5d",l);

            }
            System.out.println("\n");
        }
*/
        return a;
    }
}