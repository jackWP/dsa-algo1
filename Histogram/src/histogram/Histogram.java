/*
 * Auther - Jack Warner-Pankhurst - 100199337
 * File - Histogram.java
 * Last updated - 27/11/2018
 * Program - Outputs an array of the distribution of n random numbers 
 *           between 1-100, in groupings of 10 (1-10,11-20...91-100)
*/
package histogram;
import java.util.concurrent.ThreadLocalRandom; //for getting random numbers

public class Histogram {
//--------------------------------Main--------------------------------------
    public static void main(String[] args) 
    {
        int startsize = 5000;           //start size for array to be made
        long times;                     //start time
        long timee;                     //end time
        long avrg = 0;                  //average time
        int iterations = 100;           //no of test iterations
        
        
        for(int k = startsize; k<=50000; k += 5000)//for sizes 5000 to 500000
        {
            for (int j = 0; j<iterations;j++)         //run method 100 times
            {
                int[] array = new int[k];    //input array of input size

                for (int i =0; i<k;i++)
                {//generate random number between 1 and 100 to fill array
                    array[i] = ThreadLocalRandom.current().nextInt(1,101);
                }

                times = System.nanoTime(); //--------START
                int[] amount = Histogram(array);//------------------RUN
                timee = System.nanoTime();//---------END
                avrg += (timee - times);//Calculate time, add to average total
                
                
                System.out.println("Run "+ j + " for size "+k+" :");
                System.out.println("________________________");//run serperator


                for (int l = 0; l <10; l++)//for ever value in amount
                {
                    //new builder for *'s
                    StringBuilder sb  = new StringBuilder(""); 
                    //print groupings
                    System.out.printf("%02d - %03d | ",((l+1)*10)-9,(l+1)*10);
                    
                    for (int o = 0; o < amount[l]; o++ )
                    {
                        sb.append("*"); // append * into sb
                    }
                    System.out.print(sb + "\n"); //print no. of * onto line
                    
                }

                System.out.println("________________________");//end of run
            }
            avrg /= (double)iterations; //calculate average accross all runs
            System.out.println(avrg); //print average
        }
    }
 //-----------------------Histogram generator-------------------------
    /**
     * Generates an array of random numbers between 1-100, filling the array
     * up to its size. 
     * @param array : int array that contains values to count groupings on
     * @return int[] : filled int array of counted groupings
     */
    public static int[] Histogram(int[] array)
    {    
        int amount[] = new int[10]; //array to hold each of 10 groupings totals

        for (int i : array)//for each int in array
        { 
            amount[(int)((i-1)/10)]++;//one to one mapping of index and grouping    
        }
        return amount; //return filled results
    }
}